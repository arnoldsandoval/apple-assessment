# store.apple.com UI Assessment

* * *

## Hello.
A few things to note, the final "production" code can be found in /dist, source files are in /app.

You can view the build in your browser [here](http://www.arnoldsandoval.me/apple/).

That's pretty much it. Thanks for taking the time to review this assessment, and I hope to speak with you folks soon!

Best,

Arnold Sandoval  
[arnold@somewhereinsf.com](mailto:arnold@somewhereinsf.com)  
415.786.7077

* * *

## Utilities used:
- [Grunt](http://gruntjs.com)
- [Yeoman](http://yeoman.io)/[generator-h5bp](https://npmjs.org/package/generator-h5bp) 
- [Bower](http://bower.io) ([jQuery](http://jquery.com))
- [SaSS](http://sass-lang.com)/[Compass](http://compass-style.org) (just one mixin is used for border-box)
- [Modernizr](http://modernizr.com) Custom Build (used only for placeholder test, bower doesn't handle custom modernizr builds - so I included this separately)


