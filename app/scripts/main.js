'use strict';

function LoginForm(args) {
	var self = this;

	self.$form = args.$form;
	self.msgMissingField = args.msgMissingField;
	self.msgError = args.msgError;
	self.msgSuccess = args.msgSuccess;

	self.init();
}

LoginForm.prototype = {
    constructor: LoginForm,
	$form: null,
	msgMissingField: null,
	init: function(){

		var self = this;

		self.inputPlaceholder();

		self.$form.on('submit', function(e){
			e.preventDefault();
			self.validate();
		});

	},
	validate: function(){
		
		var i,
			self = this,
			fields = self.$form.find('input[required]');

		for( i = 0; i < fields.length; i++ ) {
			var item = fields[i];
			if (item.value.length <= 0 || item.value === item.getAttribute('placeholder')){
				window.alert(self.msgMissingField);
				return false;
			}

		}

		self.submitForm();

	},
	submitForm: function(){
		var self = this,
			formData = self.$form.serialize(),
			request = $.ajax({
				url: self.$form.attr('action'),
				type: 'POST',
				data: formData
			});

		request.done(function() {
			console.log(self.msgError);
		});

		request.fail(function() {
			console.log(self.msgError);
		});

	},

	inputPlaceholder: function(){

		// Use modernizr to test for placeholder support. 
		// Create a fallback that emulates supported browser behavior.

		if(!Modernizr.input.placeholder){

			var i,
				self = this,
				fields = self.$form.find('input[placeholder]'),
				$input = $('input[placeholder]');
		    
			for( i = 0; i < fields.length; i++ ) {
				
				var item = fields[i];

				if (item.value.length <= 0){
					item.value = fields[i].getAttribute('placeholder');
					item.className = 'placeholder-visible';
				}

			}

			$input.on('focus', function(){

				var defaultPlaceholder = $(this).attr('placeholder');

				if( $(this).val() === defaultPlaceholder ){
					$(this).val('').removeClass('placeholder-visible');
				}

			});

			$input.on('blur', function(){

				var defaultPlaceholder = $(this).attr('placeholder');

				if( $(this).val() === '' ){
					$(this).val(defaultPlaceholder).addClass('placeholder-visible');
				}

			});

		}
	}
};

var loginform = new LoginForm({
	$form: $('#store-login'),
	msgMissingField: 'Please ensure you have entered your Apple ID and Password.',
	msgError: 'failed',
	msgSuccess: 'succeeded'
});